<?php get_header(); 
if ( ! is_user_logged_in() ) {
    wp_redirect( 'https://academia.do/login' );
        exit();
}

?>

<style>
    #learndash_complete_prev_topic{
		font-size: 18px;
    	font-weight: bold;
    }

    #learndash_complete_prev_topic a{
    	text-transform: uppercase;
    	text-decoration: underline;
    	color: #f55000;
    }

    @media(max-width: 768px){
    	.courseNavegation a{
    		font-size: 12px;
    	}
    	#learndash_mark_complete_button{
    		width: 100%;
    	}
    }
</style>

	<main role="main">
	<!-- section -->
	<section>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

	<?php 
		$course_id = learndash_get_course_ID();
		$courseLink = get_the_permalink($course_id);
	?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="learndashMain container">
				<!-- post title -->
				<div class="clearfix courseNavegation">
					<div class="pull-left"><a href="<?php echo $courseLink;?>">Volver al curso</a></div>
					<div class="pull-right justNext">&nbsp;&nbsp;<?php echo learndash_next_post_link(); ?></div>
					<div class="pull-right justNext"><?php echo learndash_previous_post_link(); ?>&nbsp;&nbsp;|</div>
					<div class="pull-right" style=""><a href="#" id="nextClass">Completar y Continuar →</a></div>
				</div>
				<!-- /post title -->
				
				<?php 
				$field = get_field( "video_field" );
					if( $field ) {
					    echo '<div class="learndashVideo">'.$field.'</div>';
					} 
				?>
		
				<div class="learndashContent annotation-content">
					<h2><?php echo $lesson_course->post_title;?></h2>
					<h1 class="learndashTitle"><?php the_title(); ?></h1>
					<?php the_content();?>
					<div class="row learndashResources">
						<div class="col-sm-1 col-xs-2">
							<img src="<?php the_field('resource_icon');?>" alt="">
						</div>
						<div class="col-md-10 col-xs-10">
							<h3><?php the_field('resource'); ?></h3>
							<?php 
							$field = get_field( "resource_link" );
								if( $field ) {
								    echo '<a href="'.$field.'">Descargar PDF</a>';
								} 
							?>

						</div>
					</div>
				</div>
			</div>
		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php endif; ?>

	</section>
	<!-- /section -->
	</main>

	<script>
	    $("a#nextClass").hide();
        $(".next-link").text("→");
        $(".prev-link").text("←");
        $("#sfwd-mark-complete").insertAfter(".learndashResources");
        $("a#nextClass").click(function(event){
            event.preventDefault();
            $("#sfwd-mark-complete").submit();
        });
        if ($("#learndash_mark_complete_button").length){
            $("a#nextClass").show();
            $(".justNext").hide();
            $("#learndash_mark_complete_button").val("Completar y Continuar →")
        }
        if ($("#learndash_complete_prev_topic").length){
            $(".justNext").hide();
            $(".learndashVideo").hide();
        }

</script>

<?php get_footer(); ?>
 