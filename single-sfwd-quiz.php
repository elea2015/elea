<?php get_header();
if ( ! is_user_logged_in() ) {
    wp_redirect( 'https://academia.do/login' );
        exit();
}
?>

	<main role="main" class="defaultMain">
	<!-- section -->
	<section>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article class="container py-4 my-5" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

			<div class="row justify-content-center">
				<div class="col-8">
					<h1 class="pageTitle blue mb-3"><?php the_title();?></h1>
					<!-- post thumbnail -->
					<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
						<?php the_post_thumbnail(); // Fullsize image for the single post ?>
					<?php endif; ?>
					<!-- /post thumbnail -->
		
					<!-- post title -->
					<!-- /post title -->
					<?php the_content(); // Dynamic Content ?>
		
					<?php edit_post_link(); // Always handy to have Edit Post Links available ?>
		
					<?php //comments_template(); ?>
				</div>
			</div>

		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php else: ?>

		<!-- article -->
		<article>

			<h1><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h1>

		</article>
		<!-- /article -->

	<?php endif; ?>

	</section>
	<!-- /section -->
	</main>

<?php get_footer(); ?>
