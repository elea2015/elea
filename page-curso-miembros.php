<?php /* Template Name: Curso Miembros */;?>
<?php get_header(); ?>

    <main role="main">
        <?php get_template_part('include/curso-miembros/intro');?>
        <?php get_template_part('include/curso-miembros/lecciones');?>
        <br><br><br>
        <?php get_template_part('include/curso-miembros/workbook');?>
    </main>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>