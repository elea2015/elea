<?php /* Template Name: Detalle Curso */;?>
<?php get_header(); ?>

<main role="main">
    <?php get_template_part('include/curso-detalle/curso-intro');?>
    <?php get_template_part('include/xavier-info');?>
    <?php get_template_part('include/curso-detalle/curso-lecciones');?>
    <?php //get_template_part('include/curso-detalle/curso-features');?>
    <?php get_template_part('include/testimonios');?>
</main>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>