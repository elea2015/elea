<?php /* Template Name: editar Perfil */;?>
<?php get_header(); ?>
<?php
remove_action('wpua_before_avatar', 'wpua_do_before_avatar');
remove_action('wpua_after_avatar', 'wpua_do_after_avatar');
?>
<div class="container">
	<div class="row">
		<div class="col-12">
			<h1 class="blue mt-4 mb-2">Editar Perfil</h1>
		</div>
	</div>
</div>
<div class="container editProfile">
	<div class="row">
		<div class="col-md-4 text-center">
			<h4 class="blue">Imagen de perfil</h4>
			<?php echo do_shortcode('[avatar_upload]');?>
			<br><br>
			<?php //echo do_shortcode('[memb_actionset_button redirect_url="'.get_home_url().'/login" action_id=213 button_url='.get_site_url().'/wp-content/uploads/2017/04/cancel.png]') ?>
		</div>
		<div class="col-md-8">
			<div class="row justify-content-center">
				<div class="col-md-10">
					<h4 class="blue">Cambiar nombre</h4>
					<?php the_field('editar_perfil');?>
					<h4 class="blue">Cambiar contraseña</h4>
					<?php echo do_shortcode('[memb_change_password password1label= "Nueva contraseña:" password2label="Confirmar nueva contraseña:" buttontext="Actualizar contraseña"]'); ?>
				</div>
			</div>
		</div>
	</div>
</div>

<script>
	$( document ).ready(function() {
    console.log( "ready!" );

    $( "#wpua-upload-existing" ).replaceWith( '<button type="submit" class="button" id="wpua-upload-existing" name="submit" value="Upload">Cargar</button>' );

    $('.wpua-edit [type="submit"]').val('Actualizar imagen');

	if ($('.password_change_message:contains("Successfully")').length > 0) {
		$(".password_change_message").text("Contraseña actualizada");
	} else {
		$(".password_change_message").text("Error: Intentelo nuevamente");
	}

});
</script>


<?php get_footer(); ?>