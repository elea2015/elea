<?php
/**
 * Handle manipulating and processing of image meta information.
 */
Class ImageMeta {
    /**
     * Parse the iptc info and retrieve the given value.
     *
     * @param $value String The item you want returned
     * @param $image String The image you want info from
     * @return bool
     */
    public function iptcParser( $value=null, $image=null ) {

        $size = getimagesize( $image, $info );

        if ( ! isset( $info['APP13'] ) )
            return false;

        $iptc = iptcparse( $info['APP13'] );

        switch($value) {
            case 'keywords':
                return $iptc['2#025'];
            case 'city':
                return $iptc['2#090'][0];
            case 'region':
                return $iptc['2#095'][0];
            case 'country':
                return $iptc['2#101'][0];
            default:
                return false;
        }
    }
}