<?php 
$account = isset($_GET['account']) ? $_GET['account'] : '';
$login = isset($_GET['login']) ? $_GET['login'] : '';
/* Template Name: Login */;?>
<?php get_header(); ?>
<main role="main" id="membLogin">
	<img class="logo" src="<?php echo get_template_directory_uri(); ?>/img/logobpd.png" alt="Logo" class="logo-img">
    <section class="text-center">
        <h3 class="mb-2">Iniciar sesión</h3>
        <?php if(isset($account)):?>
            <?php if($account == "yes"):?>
            <div class="alert alert-success" role="alert">
                Contraseña enviada
            </div>
            <?php endif;?>
        <?php endif;?>

        <?php if(isset($login)):?>
            <?php if($login == "failed"):?>
            <div class="alert alert-warning" role="alert">
                Contraseña incorrecta
            </div>
            <style>.memberium-login-error{display:none}</style>
            <?php endif;?>
        <?php endif;?>

        <?php echo do_shortcode('[memb_loginform username_label="Correo electrónico" remember_label="Recuérdame" button_label="Iniciar sesión" password_label="Contraseña" redirect="/perfil/"]') ?>
        <a class="forgotPassword" href="/recuperar-contrasena/" title="Lost Password">¿Olvidaste tu contraseña?</a>
        <br>
        <a class="forgotPassword" href="/checkout/?add-to-cart=706" title="Lost Password">Crear cuenta</a>
    </section>
</main>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>