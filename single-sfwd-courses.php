<?php get_header(); 
// if ( ! is_user_logged_in() ) {
//     wp_redirect( 'https://academia.do/login' );
//         exit();
// }

?>

<?php get_template_part('include/curso-miembros/intro');?>

	<main role="main">
	<!-- section -->
	<section>

	<?php if (have_posts()): while (have_posts()) : the_post(); ?>

		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="learndashMain">
    			<div class="curso-lecciones lessons-browser">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-12 text-center">
                                <?php the_field('descripcion');?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <h1 class="title">Contenido</h1>
                                <?php echo do_shortcode('[memb_set_prohibited_action action=show]');?>
                            </div>
                        </div>
                        <?php
                        $course_id = get_the_id();
                        if( !empty($lessonsAccordion) ) : $i=0;?>
                            <div class="<?php if($course_id == 1833) {echo 'accordion';} ?> lecciones-list">
                                <?php foreach( $lessonsAccordion as $lesson ) : $i++;?>
                                    <!-- Accordion item-->
                                    <div class="row lesson-item align-items-center<?php if(empty($lesson['topics'])) echo " notopics";?>">
                                        <div class="col-1 text-center"><p class="lesson-number"><?php echo $i;?></p></div>
                                        <div class="col-11 leccionDescripcion">
                                            <?php echo '<a href="' .$lesson['link']. '">'?>
                                            <h3 class="lesson-title">
                                                <?php echo $lesson['title'];?>
                                                <?php if($lesson['status'] == 'completed') : ;?>
                                                    <i class="fa fa-check-square" aria-hidden="true"></i>
                                                <?php endif;?>
                                            </h3>
                                            <?php echo '</a>'?>
                                            <div class="lesson-description d-none">
                                                <?php echo $lesson['description'];?>
                                            </div>
                                        </div>
                                    </div>
                                    <?php if( !empty($lesson['topics'])) { $noTopics = true;} ?>
                                    <?php if ($notopics == false):?>
                                    <div class="row justify-content-center lesson-index">
                                        <?php if( !empty($lesson['topics'])) : ;?>
                                            <div class="col-sm-10 col-sm-offset-1 col-xs-10 col-xs-offset-1">
                                                <ul>
                                                    <?php foreach($lesson['topics'] as $topic) : $topic_icon = get_field("topic_icon", $topic->ID);?>
                                                        <?php $is_topic_complete = learndash_is_topic_complete(null, $topic->ID);?>
                                                        <li class="row lesson-chapter">
                                                            <?php
                                                            $post = get_post( $topic );
                                                            if ( $post->post_type == 'sfwd-topic' ) {
                                                                $lesson = learndash_get_setting($post, 'topic');
                                                                $link = get_permalink($lesson);
                                                            }
                                                            ?>
                                                            <a href="<?php echo $link;?>" title="<?php echo $topic->post_title;?>">
                                                                <div class="col-sm-11">
                                                                    <h3 class="chapter-title<?php if($is_topic_complete) echo " completed";?>">
                                                                        <!-- <img src="<?php //echo $topic_icon;?>"> -->
                                                                        <span><?php echo $topic->post_title;?></span>
                                                                        <?php if($is_topic_complete) : ;?>
                                                                            <i class="fa fa-check-square" aria-hidden="true"></i>
                                                                        <?php endif;?>
                                                                    </h3>
                                                                </div>
                                                            </a>
                                                        </li>
                                                    <?php endforeach;?>
                                                </ul>
                                            </div>
                                        <?php endif;?>
                                    </div>
                                <?php endif;?>
                                    <!--/ Accordion item-->
                                <?php endforeach;?>
                        <?php endif;?><?php wp_reset_postdata(); ?>
                    </div>
                </div>
               
                <!-- Workbook -->
                <?php 
                if ( is_user_logged_in() ):
                if(get_field("workbook")) :?>
                <div id="workbook" class="curso-lecciones workbook">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-12">
                                <h1 class="title">Material de apoyo</h1>
                            </div>
                        </div>
                        <ul class="lecciones-list">
                            <li class="row">
                                <div class="col-2 col-md-1 lesson-icon">
                                    <img src="<?php echo get_template_directory_uri(); ?>/img/icons/pdf.png" alt="">
                                </div>
                                <div class="col-10 col-md-11">
                                    <h3 class="lesson-title">Documentos</h3>
                                    <p class="lesson-description">
                                        Descárgalo y utilízalo para reforzar tu aprendizaje.
                                    </p>
                                    <?php if ( is_user_logged_in() ): ?>
                                        <?php if( have_rows('workbook') ): while ( have_rows('workbook') ) : the_row(); ?>
                                            <?php 
                                                $doc = get_sub_field('archivo');
                                                $doc_name = $doc["title"];
                                                $doc_link = $doc["url"];
                                            ?>
                                             <p>
                                                <a target="_blank" href="<?php echo $doc_link; ?>"><?php echo $doc_name ;?></a>
                                             </p>                   

                                        <?php endwhile;endif;?>
                                    <?php endif;?>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
                <?php endif;endif;?>


			</div>
		</article>
		<!-- /article -->

	<?php endwhile; ?>

    <?php endif; ?>
    
    <a class="btn btn-blue mx-auto mb-5" href="/#courses">Ver todos los cursos</a>

	</section>
	<!-- /section -->
	</main>
   

<?php get_footer(); ?>

