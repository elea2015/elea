<?php /* Template name: soporte */ get_header(); ?>

	<section class="py-5">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <h1 class="text-center"><?php the_title();?></h1>
                    <p class="text-center blue">Preguntas frecuentes</p>
                    <div class="faqsRow">
                        <div class="faqs">
                        <?php
                            // check if the repeater field has rows of data
                            if( have_rows('preguntas_frecuentes') ): while ( have_rows('preguntas_frecuentes') ) : the_row(); $i++;
                            ?>
                                <div class="faqsBox">
                                    <a class="collapseButton <?php if($q !== 1) echo 'collapsed'?> " data-toggle="collapse" href="#" data-target="#q<?php echo $i?>" role="button" aria-expanded="false" aria-controls="q<?php echo $i?>">
                                        <span class="collapse1"><?php the_sub_field('faq_pregunta'); ?></span><span class="spanPlus"><i class="fa fa-plus"></i></span><span class="spanMinus"><i class="fa fa-minus"></i></span>
                                    </a>
                                    <div class="collapse" id="q<?php echo $i?>">
                                        <div class="card card-body card-body-faqs">
                                            <p><?php the_sub_field('faq_respuesta'); ?></p>
                                        </div>
                                    </div>
                                </div>
                            <?php  endwhile; endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center my-5">
                <div class="col-md-6">
                    <h1 class="text-center">Contacto</h1>
                    <!--<p class="text-center blue">Por favor, complete el siguiente formulario para cualquier consulta.</p>-->
                    <?php echo do_shortcode('[gravityform id="2" title="false" description="true"]')?>
                </div>
            </div>
        </div>
    </section>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
