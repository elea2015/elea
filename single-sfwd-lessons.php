<?php get_header(); 
if ( ! is_user_logged_in() ) {
    wp_redirect( 'https://academia.do/login' );
        exit();
}
?>

	<main role="main">
	<!-- section -->
	<section>

	<?php if (have_posts()): while (have_posts()) : the_post();?>
	
	<?php 
		$course_id = learndash_get_course_ID();
		$courseLink = get_the_permalink($course_id);
	?>


		<!-- article -->
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<div class="learndashMain container">
				<div class="clearfix courseNavegation">
					<div class="pull-left"><a href="<?php echo $courseLink ; ?>">Volver al curso</a></div>
					<div class="pull-right d-none">&nbsp;&nbsp;<?php echo learndash_next_post_link(); ?></div>
					<!-- <div class="pull-right"><?php //echo learndash_previous_post_link(); ?>&nbsp;&nbsp;|</div> -->
				</div>
				<?php 
				$field = get_field( "video_field" );
					if( $field ) {
					    echo '<div class="learndashVideo">'.$field.'</div>';
					} 
				?>
				<div class="learndashContent annotation-content">
					<h1 class="learndashTitle"><?php the_title(); ?></h1>
					<?php the_content();?>
					<div class="row learndashResources mt-5">
						<div class="col-sm-1 col-xs-2">
							<img src="<?php the_field('resource_icon');?>" alt="">
						</div>
						<div class="col-10">
							<h3><?php the_field('resource'); ?></h3>
							<?php 
							$field = get_field( "resource_link" );
								if( $field ) {
								    echo '<a class="btn btn-white" href="'.$field.'">Descargar</a>';
								} 
							?>

						</div>
					</div>
				</div>

				<?php get_template_part('include/lesson-topics');?>
			</div>



			<div class="learndashComments"><?php //comments_template(); ?></div>

		</article>
		<!-- /article -->

	<?php endwhile; ?>

	<?php endif; ?>

	</section>
	<!-- /section -->
	</main>

<?php get_footer(); ?>
