<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
		<script>
        // conditionizr.com
        // configure environment tests
        conditionizr.config({
            assets: '<?php echo get_template_directory_uri(); ?>',
            tests: {}
        });
        </script>
	<link href="https://fonts.googleapis.com/css?family=Merriweather:400,700|Roboto:400,500,700" rel="stylesheet"> 
	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="wrapper">
			<?php if(!is_page_template(array('page-login.php', 'page-registro.php', 'page-password.php'))) : ;?>
				<!-- header -->
				<header class="header clear fixed-top" role="banner" data-spy="affix">
					<div id="top-header">
						<div class="container">
							<div class="row align-items-center">
								<div class="col-7 pr-0"><a href="<?php echo home_url(); ?>"><img class="logo" src="<?php echo get_template_directory_uri(); ?>/img/logobpd.png" alt="Logo" class="logo-img"></a></div>
								<div class="col-5"><?php html5blank_nav() ?></div>
							</div>
						</div>
					</div>
				</header>
				<!-- /header -->
		<?php endif;?>
