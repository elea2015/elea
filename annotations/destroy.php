<?php
require('functions.php');

$delData = get_delete_data();

if(!empty($delData)){
    require('../../../../wp-blog-header.php');
    require('db.php');

    global $wpdb;

    $id = $delData['id'];

    if($wpdb->delete(
        $db_table,
        array(
            'id' => $id,
            'user' => $delData['user'],
            'uri' => $delData['uri']
        ),
        array(
            '%d',
            '%d',
            '%s'
        )
    ))
        header_noContent();
    else
        header_notFound();

} else {
    header_notFound();
}