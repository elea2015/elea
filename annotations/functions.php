<?php

/**
 * @param $annotation
 * @param bool $return_in_array Var to fix the Search action bug from annotator.js lib where it receives a single annotation within an array
 * @return array|null
 */
function map_annotation($annotation, $return_in_array = false) {
    if(empty( $annotation )) {
        return null;
    }

    if (is_object($annotation)) {
        $annotation = (array) $annotation;
    }

    if (is_array($annotation)) {
        $map = array();
        $map['id'] = $annotation['id'];
        $map['user'] = $annotation['user'];
        $map['quote'] = $annotation['quote'];
        $map['text'] = $annotation['text'];
        $map['uri'] = $annotation['uri'];
        $map['created'] = $annotation['created'];
        $map['updated'] = $annotation['updated'];
        $map['ranges'] = [
            [
                'start' => $annotation['range_start'],
                'end' => $annotation['range_end'],
                'startOffset' => $annotation['start_offset'],
                'endOffset' => $annotation['end_offset'],
            ]
        ];

        if($return_in_array)
            return array($map);
        else
            return $map;
    }
    return $annotation;
}

/**
 * @param $annotations
 * @param bool $return_in_array Var to fix the Search action bug from annotator.js lib where it receives a single annotation within an array
 * @return array|null
 */
function map_annotations($annotations, $return_in_array = false) {
    if (count($annotations) < 2) {
        return map_annotation($annotations[0], $return_in_array);
    }

    if (is_object($annotations)) {
        $annotations = (array) $annotations;
    }

    if (is_array($annotations)) {
        $map = array();
        $i=0;
        foreach($annotations as $annotation) {
            $ann = $annotation;

            if (is_object($ann)) {
                $ann = (array) $ann;
            }

            $map[$i]['id'] = $ann['id'];
            $map[$i]['user'] = $ann['user'];
            $map[$i]['quote'] = $ann['quote'];
            $map[$i]['text'] = $ann['text'];
            $map[$i]['uri'] = $ann['uri'];
            $map[$i]['created'] = $ann['created'];
            $map[$i]['updated'] = $ann['updated'];
            $map[$i]['ranges'] = [
                [
                    'start' => $ann['range_start'],
                    'end' => $ann['range_end'],
                    'startOffset' => $ann['start_offset'],
                    'endOffset' => $ann['end_offset'],
                ]
            ];
            $i++;
        }
        return $map;
    }
    return $annotations;
}

//Function to fix up PHP's messing up POST input containing dots, etc.
function get_post_data() {
    $post_vars = explode("&", file_get_contents("php://input"));

//    $vars = array();
//    print_r($pairs);
//    foreach ($pairs as $pair) {
//        $nv = explode("=", $pair);
//        $name = urldecode($nv[0]);
//        $value = urldecode($nv[1]);
//        $vars[$name] = $value;
//    }
//    return $vars;

    $post_vars = json_decode($post_vars[0]);

    return $post_vars;
}

function get_put_data() {
    if($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $post_vars = file_get_contents("php://input");

        $post_vars = json_decode($post_vars, true);

        return $post_vars;
    }
    return null;
}

function get_delete_data() {
    if($_SERVER['REQUEST_METHOD'] == 'DELETE') {
        $post_vars = file_get_contents("php://input");

        $post_vars = json_decode($post_vars, true);

        print_r($post_vars);

        return $post_vars;
    }
    return null;
}

function header_ok_json() {
    header("HTTP/1.1 200 OK");
    header('Content-Type: application/json');
}

function header_notFound() {
    header("HTTP/1.0 404 Not Found");
    exit();
}

function header_seeOther($id) {
    header("HTTP/1.1 303 See Other");
    header("Location: read.php?id=$id");
    exit();
}

function header_noContent() {
    header("HTTP/1.0 204 No Content");
    exit();
}