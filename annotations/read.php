<?php
if($_GET){
    require('../../../../wp-blog-header.php');
    require('db.php');
    require('functions.php');

    global $wpdb;

    $annotation = $wpdb->get_row("SELECT * FROM ".$db_table." WHERE id=". $_GET['id'] . " AND user=".get_current_user_id());

    $annotation = map_annotation($annotation);

    header_ok_json();

    echo json_encode($annotation);

    exit();
}
else {
    header_notFound();
}