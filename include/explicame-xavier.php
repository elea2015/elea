<?php
$course_id = get_field("curso") ? get_field("curso") : 555;
$lessons = learndash_get_course_lessons_list( $course_id );
$lessonsCount = count($lessons);
$completedLessons = 0;
if (!empty($lessons)) {
    /** @var array $lessons */
    foreach($lessons as $lesson)
        if ($lesson['status'] == 'completed') $completedLessons++;
}
?>
<div class="explicame">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="half-left link-height">
                   <a href="<?php echo get_home_url(); ?>/page-curso-miembros/"><div class="explicameContent">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/curso1.jpg" alt="">
                       </div></a>
                </div>
                <div class=" half-right link-height">
                    <?php if(is_front_page()) : ?>
                        <a href="<?php echo get_home_url(); ?>/curso/">
                            <h3 class="cursoTitulo">Contablilidad para Pymes</h3>
                        </a>
                        <p>
                            Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                            mus. Nulla vitae elit libero, a pharetra augue. Nulla vitae elit libero, a pharetra augue.
                        </p>
                        <a href="<?php echo get_home_url(); ?>/curso/" class="btn btn-orange">Tomar este curso</a>
                    <?php else : ?>
                        <p class="text-center text-uppercase">
                            Tu progreso<br>
                            <span class="tu-progreso"><?php echo sprintf("%02d/%02d", $completedLessons, $lessonsCount);?></span>
                            Lecciones completadas
                        </p>
                        <div class="text-center learndash-resume-button">
                           <?php echo do_shortcode('[uo_learndash_resume]')?>
                        </div>
                    <?php endif;?>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="half-left link-height">
                   <a href="<?php echo get_home_url(); ?>/page-curso-miembros/"><div class="explicameContent">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/curso2.jpg" alt="">
                       </div></a>
                </div>
                <div class=" half-right link-height">
                    <?php if(is_front_page()) : ?>
                        <a href="<?php echo get_home_url(); ?>/curso/">
                            <h3 class="cursoTitulo">Mercadeo en Linea</h3>
                        </a>
                        <p>
                            Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                            mus. Nulla vitae elit libero, a pharetra augue. Nulla vitae elit libero, a pharetra augue.
                        </p>
                        <a href="<?php echo get_home_url(); ?>/curso/" class="btn btn-orange">Tomar este curso</a>
                    <?php else : ?>
                        <p class="text-center text-uppercase">
                            Tu progreso<br>
                            <span class="tu-progreso"><?php echo sprintf("%02d/%02d", $completedLessons, $lessonsCount);?></span>
                            Lecciones completadas
                        </p>
                        <div class="text-center learndash-resume-button">
                           <?php echo do_shortcode('[uo_learndash_resume]')?>
                        </div>
                    <?php endif;?>
                </div>
            </div>

            <div class="col-sm-4">
                <div class="half-left link-height">
                   <a href="<?php echo get_home_url(); ?>/page-curso-miembros/"><div class="explicameContent">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/curso3.jpg" alt="">
                       </div></a>
                </div>
                <div class=" half-right link-height">
                    <?php if(is_front_page()) : ?>
                        <a href="<?php echo get_home_url(); ?>/curso/">
                            <h3 class="cursoTitulo">Manejo de Equipos</h3>
                        </a>
                        <p>
                            Donec sed odio dui. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
                            mus. Nulla vitae elit libero, a pharetra augue. Nulla vitae elit libero, a pharetra augue.
                        </p>
                        <a href="<?php echo get_home_url(); ?>/curso/" class="btn btn-orange">Tomar este curso</a>
                    <?php else : ?>
                        <p class="text-center text-uppercase">
                            Tu progreso<br>
                            <span class="tu-progreso"><?php echo sprintf("%02d/%02d", $completedLessons, $lessonsCount);?></span>
                            Lecciones completadas
                        </p>
                        <div class="text-center learndash-resume-button">
                           <?php echo do_shortcode('[uo_learndash_resume]')?>
                        </div>
                    <?php endif;?>
                </div>
            </div>
        </div>
    </div>
</div>