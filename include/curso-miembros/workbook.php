<div id="workbook" class="curso-lecciones workbook">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="title">Recursos</h1>
            </div>
        </div>
        <ul class="lecciones-list">
            <li class="row">
                <div class="col-xs-1 lesson-icon">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/icons/pdf.png" alt="">
                </div>
                <div class="col-xs-11">
                    <h3 class="lesson-title">Material de apoyo</h3>
                    <p class="lesson-description">
                        Descarga el workbook y utilízalo para reforzar tu aprendizaje.
                    </p>
                    <p>
                        <a href="#" target="_blank">Descargar aqu&iacute;</a>
                    </p>
                </div>
            </li>
        </ul>
        <br>
        <br>
        <br>
       <!--  <div class="row questionsAnswers">
            <div class="col-md-8 col-md-offset-2 text-center">
                <h1>Preguntale a Xavier</h1>
                <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec ullamcorper nulla non metus auctor fringilla. Cras mattis consectetur purus sit amet fermentum. Curabitur blandit tempus porttitor.</p>
                <a href="#">Ver a Xavier respondiendo</a>
            </div>
        </div> -->
    </div>
</div>