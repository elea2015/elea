<?php
$course_id = get_field("curso") ? get_field("curso") : 555;
$lessons = learndash_get_course_lessons_list( $course_id );
$completedLessons = 0;
$lessonsCount = count($lessons);
$lessonsAccordion = [];
$count = 0;

global $lessonsAccordion;
if (!empty($lessons)) {
    /** @var array $lessons */
    foreach($lessons as $lesson) {
        $lessonsAccordion[$count]['title'] = $lesson['post']->post_title;
        $lessonsAccordion[$count]['description'] = $lesson['post']->post_content;
        $lessonsAccordion[$count]['topics'] = learndash_get_topic_list($lesson['post']->ID);
        $lessonsAccordion[$count]['link'] = get_post_permalink($lesson['post']->ID);
        

        $count++;
    }
}
?>

<?php wp_reset_query(); wp_reset_postdata(); global $lessonsAccordion;?>
<div class="curso-lecciones lessons-browser">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1 class="title">Plan de lecciones</h1>
            </div>
        </div>
        <?php if( !empty($lessonsAccordion) ) : $i=0;?>
            <div class="lecciones-list">
                <?php foreach( $lessonsAccordion as $lesson ) : $i++;?>
                    <!-- Accordion item-->
                    <div class="row lesson-item<?php if(empty($lesson['topics'])) echo " notopics";?>">
                        <div class="col-xs-1"><p class="lesson-number"><?php echo $i;?></p></div>
                        <div class="col-xs-11 leccionDescripcion">
                            <a href="<?php echo $lesson['link'];?>"><h3 class="lesson-title" style="margin-top:8px">
                                <?php echo $lesson['title'];?>
                                <?php if($lesson['status'] == 'completed') : ;?>
                                    <i class="fa fa-check-square" aria-hidden="true"></i>
                                <?php endif;?>
                            </h3></a>
                        </div>
                    </div>
                 
                    <!--/ Accordion item-->
                <?php endforeach;?>
        <?php endif;?>
    </div>
</div>