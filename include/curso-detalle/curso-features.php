<div class="curso-features">
    <div class="container">
        <div class="row features-list">
            <div class="col-sm-4">
                <div class="feature">
                    <div class="feature-icon">
                        <p>100%</p>
                    </div>
                    <h3 class="feature-title"><?php the_field('garantizado_titulo'); ?></h3>
                    <p><?php the_field('garantizado'); ?></p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="feature">
                    <div class="feature-icon">
                        <img src="<?php the_field('compatibilidad_imagen'); ?>" alt="">
                    </div>
                    <h3 class="feature-title"><?php the_field('compatibilidad_titulo'); ?></h3>
                    <p><?php the_field('compatibilidad'); ?>.</p>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="feature">
                    <div class="feature-icon">
                        <div>
                            <img src="<?php the_field('expira_imagen'); ?>" alt="">
                        </div>
                    </div>
                    <h3 class="feature-title"><?php the_field('expira_titulo'); ?></h3>
                    <p><?php the_field('expira'); ?>.</p>
                </div>
            </div>
        </div>
    </div>
</div>