<div class="curso-lecciones lessons-browser">
    <?php $temas = learndash_get_topic_list(get_the_ID());?>
    <?php if( !empty($temas) ) : ;?>
        <div class="row">
            <div class="col-sm-12">
                <h1 class="title">Temas</h1>
            </div>
        </div>
        <div class="lecciones-list">
            <div class="row lesson-index">
                <ul class="w-100">
                    <?php foreach( $temas as $topic ) : $topic_icon = get_field("topic_icon", $topic->ID);?>
                        <?php $is_topic_complete = learndash_is_topic_complete(null, $topic->ID);?>
                        <?php
                        $post = get_post( $topic );
                        if ( $post->post_type == 'sfwd-topic' ) {
                            $lesson = learndash_get_setting($post, 'topic');
                            $link = get_permalink($lesson);
                        }
                        ?>
                        <li class="row justify-content-center lesson-chapter">
                            <a href="<?php echo $link;?>" title="<?php echo $topic->post_title;?>">
                                <div class="col-sm-11">
                                    <h3 class="chapter-title<?php if($is_topic_complete) echo " completed";?>">
                                        <!-- <img src="<?php //echo $topic_icon;?>"> -->
                                        <span><?php echo $topic->post_title;?></span>
                                        <?php if($is_topic_complete) : ;?>
                                            <i class="fa fa-check-square" aria-hidden="true"></i>
                                        <?php endif;?>
                                    </h3>
                                </div>
                            </a>
                        </li>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
    <?php endif;?>
</div>