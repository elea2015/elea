<div class="xavier-info">
    <img class="onlyMobile" src="<?php echo get_template_directory_uri(); ?>/img/xs-8.jpg" alt="">
    <div class="container pad-bot">
        <div class="row">
            <div class="col-sm-6 half-left">
                <h1 class="subtitle">Del lenguaje financiero al espa&ntilde;ol.</h1>
                <p>
                    Xavier Serbiá, economista y presentador del programa CNN Dinero de CNN en Español, conferencista y autor
                    bestseller de “La Riqueza en 4 Pisos” y “Pregúntale a Xavier”, por años ha tenido la oportunidad de
                    hablar ante miles de personas sobre la importancia de saber administrar el dinero.
                    La misión de Xavier es la democratización del conocimiento financiero y los negocios, para hacerlos
                    accesibles a todos.
                </p>
            </div>
        </div>
    </div>
</div>