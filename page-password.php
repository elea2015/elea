<?php /* Template Name: Passwoord */;?>
<?php get_header(); ?>

<style type="text/css">
	.header, .footer{display: none;}
	#memb_password_send-1-block2{margin-top: 10px;}
</style>
<main role="main" id="membLogin">
    <img class="logo" src="<?php echo get_template_directory_uri(); ?>/img/logobpd.png" alt="Logo" class="logo-img">
    <section class="text-center">
    	<h3>Recuperar contraseña</h3>
    	<br>
        <?php echo do_shortcode('[memb_send_password successurl="/login/?account=yes" template_id="167" emailtext="Correo electrónico" buttontext="Solicitar contraseña"]') ?><br>
        <a class="forgotPassword" href="/login/" title="Lost Password">Iniciar sesión</a>
    </section>
</main>


<script>
	$( document ).ready(function() {
    // console.log( "ready!" );

    // $( "#memb_password_send-1-email-label" ).replaceWith( '<label id="memb_password_send-1-email-input">Correo electrónico</label>' );

    // $('input[type=submit]').val('Solicitar contraseña');

    $( ".password_send_message" ).replaceWith( '<div class="alert alert-danger" role="alert">Cuenta no existe, intente nuevamente</div>' );

});
</script>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>